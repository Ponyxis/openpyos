# SPDX-License-Identifier: GPL-3.0-only
import gc

from lib.core.commands import command_list

class Shell:
    def __init__(self, input_instance, output_instance):
        self.output_stream = output_instance
        self.input_stream = input_instance
        self.data_from_keyboard_controller = bytearray(2)
        self.input_line = ""
        self.parse_line = ""
        self.parse_input_args = ""

    def main(self):
        if self.input_stream.available():
            size = self.input_stream.read(self.data_from_keyboard_controller, 2)
            index = 0
            if index < size:
                if self.data_from_keyboard_controller[0] == 0x0D:  # enter key
                    if len(self.input_line) != 0: # Input buffer is not empty. Process input.
                        self.parse_line = self.input_line.split()

                        if self.parse_line[0] in command_list:
                            if len(self.parse_line) > 1: # Command include arguments after name
                                self.parse_input_args = self.parse_line[1].split(" ")
                            command_list[self.parse_line[0]](len(self.parse_input_args), self.parse_input_args)
                        else:
                            self.output_stream.new_line()
                            self.output_stream.write(f"{self.parse_line[0]}: command not found")

                        self.output_stream.write(">>")
                    else:
                        self.output_stream.new_line()
                        self.output_stream.write(">>")
                    self.input_line = ""
                    self.parse_line = ""
                    self.parse_input_args = ""
                    gc.collect()
                elif self.data_from_keyboard_controller[0] == 0x08: # backspace key
                    if len(self.input_line) > 0:
                        self.input_line = self.input_line[:-1]
                        self.output_stream.delete_character()
                else:
                    self.output_stream.write(chr(self.data_from_keyboard_controller[0]))
                    self.input_line += chr(self.data_from_keyboard_controller[0])
