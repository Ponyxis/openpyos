# SPDX-License-Identifier: GPL-3.0-only
from machine import UART


class Nextion:
    def __init__(self, uart_number, number_of_lines, uart_speed):
        self.uart = UART(uart_number, uart_speed)
        self.actual_line = 0
        self.number_of_lines = number_of_lines
        self.read_bytes = bytearray(100)
        self.read_size = 0

    def __send(self, input_data):
        write_buffer = bytearray(len(input_data) + 3)
        write_buffer[0: len(input_data)] = bytes(input_data, 'ASCII')
        write_buffer[-3:] = b'\xff\xff\xff'
        # print(write_buffer)
        self.uart.write(write_buffer)

    def __scroll(self):
        for line in range(0, self.number_of_lines):
            self.__send(f't{line}.txt=t{line+1}.txt')
        self.__send('t{}.txt=""'.format(self.number_of_lines))

    def read_loop(self, callback):
        if self.uart.any():
            self.read_size = self.uart.readinto(self.read_bytes, 100)
            index = 0
            if index < self.read_size:
                callback(read_data=self.read_bytes[:self.read_size])

    def write(self, *args):
        if self.actual_line > self.number_of_lines:
            self.__scroll()
            self.actual_line = self.actual_line - 1
        for arg in args:
            self.__send(f't{self.actual_line}.txt+="{arg}"')

    def write_line(self, line_number, *args):
        if line_number <= self.number_of_lines:
            for arg in args:
                self.__send(f't{line_number}.txt+="{arg}"')

    def new_line(self):
        self.actual_line = self.actual_line + 1

    def show_page(self, page_name):
        self.__send(f"page {page_name}")

    def delete_character(self):
        self.__send(f't{self.actual_line}.txt=t{self.actual_line}.txt-1')

    def clear_screen(self):
        for line in range(0, self.number_of_lines):
            self.__send(f't{line}.txt=""')
        self.actual_line = 0


output_stream = Nextion(8, 29, 512000)
