# SPDX-License-Identifier: GPL-3.0-only
from machine import UART


class InputStream:
    def __init__(self, uart_instance, uart_speed):
        self.uart = UART(uart_instance, uart_speed)

    def read(self, buffer_to_read, buffer_size):
        return self.uart.readinto(buffer_to_read, buffer_size)

    def available(self):
        return self.uart.any()


input_stream = InputStream(7, 115200)
