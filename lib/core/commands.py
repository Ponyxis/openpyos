# SPDX-License-Identifier: GPL-3.0-only
from bin.programs import programs_main
from bin.free import free_main
from bin.mem_stat import mem_stat_main
from bin.ls import ls_main

command_list = {
    "programs": programs_main,
    "free": free_main,
    "mem-stat": mem_stat_main,
    "ls": ls_main
}
