# SPDX-License-Identifier: GPL-3.0-only
from lib.core.nextion import output_stream
import gc

def mem_stat_main(argc, args):
    output_stream.new_line()
    output_stream.write(f"Used RAM: {gc.mem_alloc()} bytes")
    output_stream.new_line()
    output_stream.write(f"Free RAM: {gc.mem_free()} bytes")
    output_stream.new_line()
