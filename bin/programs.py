# SPDX-License-Identifier: GPL-3.0-only
from lib.core.nextion import output_stream

def programs_main(argc, args):
    output_stream.new_line()
    output_stream.write("programs free mem-stat ls")
    output_stream.new_line()
