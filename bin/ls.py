# SPDX-License-Identifier: GPL-3.0-only
from lib.core.nextion import output_stream
import uos

def ls_main(argc, args):
    output_stream.new_line()
    path = uos.getcwd()
    for l in uos.ilistdir(path):
        if l[0][0] != '.':
            output_stream.write(f'{l[0]}')
            output_stream.new_line()
