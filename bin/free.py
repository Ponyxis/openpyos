# SPDX-License-Identifier: GPL-3.0-only
from lib.core.nextion import output_stream
import gc

def free_main(argc, args):
    gc.collect()
    output_stream.new_line()
