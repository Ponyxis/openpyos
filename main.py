# SPDX-License-Identifier: GPL-3.0-only
from lib.core.nextion import output_stream
from lib.core.shell import Shell
from lib.core.input import input_stream
import time
import gc

time.sleep_ms(500)

output_stream.write("OpenPYOS v0.0.1")
output_stream.new_line()
output_stream.write(f"{gc.mem_free()} bytes free.")
output_stream.new_line()
output_stream.write(">>")

shell = Shell(input_stream, output_stream)

while True:
    shell.main()
